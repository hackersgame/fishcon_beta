import time, io, multiprocessing, os

global endPosition
global addressGroups

addressGroups = {}
endPosition = 0
OUT = open("./scanmemOUT")
IN = open("./scanmemIN", 'a')

def getPid():
  pidFile = open("./targetPID")
  pid = pidFile.readlines()[0].strip()
  pidFile.close()
  return pid


def writePid(pid):
  pidFile = open("./targetPID", 'w')
  pidFile.write(pid)
  pidFile.close()
  
  
def writeAddress(address, value, valueType):
 #TODO clean
  bashCommand = "echo 'write " + valueType + " " + str(address) + " " + str(value) + "' | scanmem -p " + str(getPid()) + " 2>/dev/null"
  #print (bashCommand)
  os.system(bashCommand)

def runScanmemCMD_raw(cmd):
  #open stdin file and write data
  IN = open("./scanmemIN", 'a')
  IN.write(cmd + "\n")
  IN.close()
  

def runScanmemCMD(cmd):
  #open stdin file and write data
  IN = open("./scanmemIN", 'a')
  IN.write(cmd + "\n")
  IN.close()
  
  #read output
  global endPosition
  OUT.seek(0, io.SEEK_END)

  timeout = 5
  timeoutCount = 0.0
  
  while True:
    if timeoutCount >= timeout:
      print("Error: scanmem timeout")
      return -1
    time.sleep(.1)
    timeoutCount = timeoutCount + .1
    while True:
      line = OUT.readline()
      if line != "":
        yield line
        timeoutCount = 0
      if ">" in line:
        return line.split(">")[0]
      newPos = OUT.tell()
      if newPos == endPosition:
        break
      else:
        endPosition = newPos


def betterList():
  runScanmemCMD_raw('update')
  time.sleep(0.1)
  data = []
  for line in runScanmemCMD('list'):
    if line.startswith("["):
      dic = {}
      dic['value'] = line.split(",")[3].strip()
      dic['matchID'] = line.split("]")[0].split(" ")[-1]
      dic['address'] = line.split(",")[0].split(" ")[-1]
      if 'I64' in line:
        dic['type'] = 'int64'
      if 'I32' in line:
        dic['type'] = 'int32'
      if 'I16' in line:
        dic['type'] = 'int16'
      if 'I8' in line:
        dic['type'] = 'int8'
      if 'F64' in line:
        dic['type'] = 'float64'
      if 'F32' in line:
        dic['type'] = 'float32'

      data.append(dic)
    
  return data
