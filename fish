#!/bin/bash

#For debug use "tail -f scanmemIN | scanmem &>> scanmemOUT &" and "./fish.py" instead of the script

#check if scanmem is still running:

#clean up
echo ""> scanmemIN
echo ""> scanmemOUT

#check if scanmem is running atm (uses pid in ./scanmemPID.txt)
if [ -f "./scanmemPID.txt" ]
then
  scanmemPID=$(cat scanmemPID.txt)
  if ps -p $scanmemPID > /dev/null
  then
    echo "scanmem is already running PID: $scanmemPID"
    ./fish.py
    echo "---------------------"
    exit
  fi
fi


echo "running scanmem"
tail -f scanmemIN | scanmem &>> scanmemOUT &
scanmemPID=$!

#update running pid
echo $scanmemPID > scanmemPID.txt
./fish.py
echo "---------------------"
#kill scanmemPID
#echo "Killed scanmem"