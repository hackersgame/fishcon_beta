import functions

  
def exit(*args):
  print("yep")
  
def pid(*args):
  if len(args) == 1 and args[0] != "":
    #functions.getNewOutput() #clear anything out that is old
    functions.writePid(args[0])
    for line in functions.runScanmemCMD("pid " + args[0]):
      print(line.strip())
    #print (functions.getNewOutput())
  else:
    print("read help... if we had one :)")
  
  
def listStuff(*args):
  print("Current Data: ")
  listData = functions.betterList()
  #print (listData)
  for i in range(0, len(listData)):
    print(str(i) + " " + str(listData[i]))
    
  for listDataKey in list(functions.addressGroups.keys()):
    print ("\n\ngroupSave: " + listDataKey)
    listData = functions.addressGroups[listDataKey]
    for i in range(0, len(listData)):
      print(str(i) + " " + str(listData[i]))

def zeldaTest(*args):
  #did we get 1 arg (aka name)?
  if len(args) == 1 and args[0] != "":
    varName = args[0]
  for i in range(1, 1000):
    print("\n\n\nTesting: " + str(i))
    setStuff(varName, i)
    functions.time.sleep(1)

def save(*args):
  #did we get 1 arg (aka name)?
  if len(args) == 1 and args[0] != "":
    name = args[0]
  else:
    name = "group" + str(len(functions.addressGroups))
    
  functions.addressGroups[name] = functions.betterList()
  print("saved as: " + name)
    

def lock(*args):
  if len(args) == 1 and args[0] != "":
    #handle ctrl + c
    try:
      while True:
        functions.runScanmemCMD_raw("set " + args[0])
        functions.time.sleep(.5)
    except KeyboardInterrupt:
      print("Done! :)")
      return
  else:
    print("read help... if we had one :)")

#todo
def setStuff(*args):
  #did we get 1 or 2 arg(s) (aka group/value)?
  useGroup = False
  groupName = ""
  if len(args) == 1 and args[0] != "":
    val = args[0]
  elif len(args) == 2:
    useGroup = True
    val = args[-1]
    groupName = args[0]
   
  if useGroup:
    if groupName in list(functions.addressGroups.keys()):
      print ("USING group")
      listData = functions.addressGroups[groupName]
    else:
      print("error: groups '" + groupName + "' not found")
      return
  else:
    listData = functions.betterList()
    
  print(listData)
  for i in range(0,len(listData)):
    functions.writeAddress(listData[i]['address'], val, listData[i]['type'])
  #functions.addressGroups[name] = functions.betterList()
  #print("saved as: " + name)

#freeze values
def freeze(*args):
  #handle ctrl + c
  try: 
    #pull new list
    try:
      listData = functions.betterList()
    except Exception:
      #something went wrong with the data pull..  exit
      return
    
    print(listData)
    #loop setting the vars
    while True:
      for i in range(0,len(listData)):
        try:
          functions.writeAddress(listData[i]['address'], listData[i]['value'], listData[i]['type'])
        except Exception:
          print ("Error using: " + str(listData[i]))
        #functions.runScanmemCMD_raw("set " +str(i) + "=" + str(listData[i]))
      functions.time.sleep(.5)
      
  except KeyboardInterrupt:
    print("Done! :)")
    return


#Restore a value
def remember(*args):
  if len(args) == 1 and args[0] != "":
    #listData = functions.addressGroups[groupName]
    try:
      listData = functions.addressGroups[args[0]]
    except Exception:
      print("Error finding group: " + args[0])


    print(listData)
    #loop setting the vars
    for i in range(0,len(listData)):
      try:
          functions.writeAddress(listData[i]['address'], listData[i]['value'], listData[i]['type'])
      except Exception:
          print ("Error using: " + str(listData[i]))
      #functions.runScanmemCMD_raw("set " +str(i) + "=" + str(listData[i]))
      #functions.time.sleep(.5)
    



def godMode(*args):
  #handle ctrl + c
  try: 
    while True:
      try:
        listData = functions.betterList()
        valueATM = int(listData[0]['value'])
        newHits = int(valueATM % 5)
      except Exception:
        #something went wrong with the data pull.. try again
        functions.time.sleep(.5)
        continue
      if newHits >= 1:
        print("We got a hit: " + str(newHits))
        print("value: " + str(valueATM))
        print("ListData: " + str(listData))
        newValue = valueATM - newHits + (newHits * 5)
        print("newValue: " + str(newValue))
        #print("Debug: before set")
        #TODO fix this
        for i in range(0,len(listData)):
          functions.writeAddress(listData[i]['address'], newValue, listData[i]['type'])

            
        #print("debug: after set")
      functions.time.sleep(.5)
  except KeyboardInterrupt:
    print("Done! :)")
    return

  
def JFJFJFJ(*args):
  for arg in args:
    print (arg)
  print("FishCon.com")
  
  
  
