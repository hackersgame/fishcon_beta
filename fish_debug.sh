#!/bin/bash


echo "" > scanmemIN
echo "" > scanmemOUT

clear
echo "run './fish.py' in a different terminal"
echo "run 'tail -f ./scanmemIN ./scanmemOUT' for more debug info."
tail -f scanmemIN | scanmem &>> scanmemOUT 

echo "\n\n"